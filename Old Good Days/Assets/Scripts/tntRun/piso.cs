using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class piso : MonoBehaviour, iTnt
{
    public float contador = 1;

    public void Destruir()
    {
        StartCoroutine(DestruirDespuesDe1Segundo());
    }

    private IEnumerator DestruirDespuesDe1Segundo()
    {
        yield return new WaitForSeconds(contador);

        Destroy(gameObject);
    }
}

