using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ganador : MonoBehaviour
{
    public static string GanadorNombre;
    public TextMeshProUGUI scoreText;
    public GameObject panel;

    private void Start()
    {
        GanadorNombre = ""; 
        scoreText.text = ""; 
    }

    private void Update()
    {
        scoreText.text = GanadorNombre;
        if (!string.IsNullOrEmpty(GanadorNombre)) 
        {
            panel.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}