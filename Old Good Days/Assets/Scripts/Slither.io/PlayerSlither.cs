using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSlither : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float dashDistance = 5f;
    public GameObject tailPrefab;
    public GameObject dashIndicatorPrefab;
    public float tailSpawnRate = 0.2f;
    public float dashCooldown = 2f;
    public float slow = 1f;
    private bool canMove = false;

    private float nextTailSpawnTime;
    private Queue<Transform> tailSectionsQueue = new Queue<Transform>();
    private int maxTailSections = 50;
    private bool canDash = true;
    private Vector3 lastMovementDirection = Vector3.forward; // Default forward direction
    public Transform respawn;

    private void Start()
    {
        nextTailSpawnTime = Time.time + tailSpawnRate;
    }

    private void Update()
    {
        if (!canMove)
        {
            if (Input.anyKeyDown)
            {
                canMove = true;
            }
            else
            {
                return; 
            }
        }

        Move();
        SpawnTail();
        Dash();
        if (canDash)
        {
            moveSpeed = 10f;
        }
    }

    private void Move()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // If there's input, update lastMovementDirection
        if (Mathf.Abs(horizontalInput) > 0.1f || Mathf.Abs(verticalInput) > 0.1f)
        {
            lastMovementDirection = new Vector3(horizontalInput, 0f, verticalInput).normalized;
        }

        Vector3 movement = lastMovementDirection * moveSpeed * Time.deltaTime;

        transform.Translate(movement);
    }

    private void SpawnTail()
    {
        if (Time.time >= nextTailSpawnTime)
        {
            if (tailSectionsQueue.Count >= maxTailSections)
            {
                Transform tailToRemove = tailSectionsQueue.Dequeue();
                Destroy(tailToRemove.gameObject);
            }

            GameObject tail = Instantiate(tailPrefab, transform.position, Quaternion.identity);
            tailSectionsQueue.Enqueue(tail.transform);

            nextTailSpawnTime = Time.time + tailSpawnRate;
        }
    }

    private void Dash()
    {
        if ((Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) && canDash)
        {
            if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f || Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f)
            {
                Vector3 dashDirection = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical")).normalized * dashDistance;
                transform.position += dashDirection;

                canDash = false;
                StartCoroutine(DashCooldown());
                GenerateDashIndicator();
            }
        }
    }

    private IEnumerator DashCooldown()
    {
        yield return new WaitForSeconds(dashCooldown);
        canDash = true;
    }
    

    private void GenerateDashIndicator()
    {
        GameObject dashIndicator = Instantiate(dashIndicatorPrefab, transform.position + Vector3.up * 2f, Quaternion.identity);
        dashIndicator.transform.parent = transform;
        moveSpeed = slow;
        Destroy(dashIndicator, 2f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("slither Azul"))
        {
            ganador.GanadorNombre="Gano el Jugador Azul!";
            Destroy(this.gameObject); // Destruir el objeto con el tag "slither Rojo"
        }
    }
}