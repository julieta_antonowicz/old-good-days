using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISeguirEmpujar : MonoBehaviour
{
    public Transform esfera;  // La esfera a perseguir
    public float velocidad = 5f; // Velocidad de persecución
    public float distanciaMinima = 1f; // Distancia mínima para detenerse y aplicar el impulso
    public float fuerzaImpulso = 10f; // Fuerza del impulso
    public Vector3 ubicacionImpulso; // Ubicación hacia donde se aplicará el impulso

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Calcular la dirección hacia el esfera
        Vector3 direccion =  (esfera.position - transform.position).normalized;

        // Calcular la distancia al esfera
        float distancia = Vector3.Distance(transform.position, esfera.position);

        // Si la distancia es mayor que la distancia mínima, persigue al esfera
        if (distancia > distanciaMinima)
        {
            // Mover el objeto hacia el esfera
            rb.velocity = direccion * velocidad;
        }
        else
        {
            // Detener el objeto
            rb.velocity = Vector3.zero;

            // Aplicar el impulso hacia la ubicación específica
            Vector3 direccionImpulso = (ubicacionImpulso - transform.position).normalized;
            rb.AddForce(direccionImpulso * fuerzaImpulso, ForceMode.Impulse);
        }
    }
}