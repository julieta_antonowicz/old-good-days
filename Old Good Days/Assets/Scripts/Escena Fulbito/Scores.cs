using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scores : MonoBehaviour
{
    public static int scoreRojo;
    public static int scoreAzul;
    public TextMeshProUGUI textoScoreRojo;
    public TextMeshProUGUI textoScoreAzul;

    public void Update()
    {
        textoScoreAzul.text = "Goles Jugador Azul : " + scoreAzul.ToString();
        textoScoreRojo.text = "Goles Jugador Rojo : " + scoreRojo.ToString();

        if(scoreRojo>=5)
        {
            ganador.GanadorNombre = "El Jugador Rojo es el ganador!";
        }
        else if(scoreAzul>=5)
        {
            ganador.GanadorNombre = "el Jugador Azul es el Ganador!";
        }
    }
}
