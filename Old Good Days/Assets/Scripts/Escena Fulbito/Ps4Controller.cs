using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ps4Controller : MonoBehaviour
{
    private string horizontalAxis = "JHorizontal";
    private string verticalAxis = "JVertical";

    public float speed = 5.0f;
    public float gravity = 14.0f;
    public float maxVelocityChange = 10.0f;
    public bool canJump = true;
    public float jumpHeight = 2.0f;

    private bool grounded = false;
    private Rigidbody r;

    public bool spaceKey;


    void Awake()
    {
        r = GetComponent<Rigidbody>();
        r.freezeRotation = true;
        r.useGravity = false;
    }
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            spaceKey = true;
        }
        if (Input.GetButtonUp("Jump"))
        {
            spaceKey = false;
        }
    }

    void FixedUpdate()
    {
        
            Vector3 targetVelocity = new Vector3(Input.GetAxis("JHorizontal"), 0, Input.GetAxis("JVertical"));
            targetVelocity = transform.TransformDirection(targetVelocity);
            targetVelocity *= speed;

            Vector3 velocity = r.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;
            r.AddForce(velocityChange, ForceMode.VelocityChange);


        if (grounded)
            if (canJump && Input.GetButton("Jump"))
            {
                r.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            }
        

        r.AddForce(new Vector3(0, -gravity * r.mass, 0));

        grounded = false;
    }

    void OnCollisionStay()
    {
        grounded = true;
    }

    float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("tntFin"))
        {
            ganador.GanadorNombre="�Gan� el Jugador rojo!";
            Destroy(this.gameObject); 
        }
    }
}