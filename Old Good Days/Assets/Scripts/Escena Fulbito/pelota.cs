using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pelota : MonoBehaviour
{
    public Transform respawn;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("arco Rojo"))
        {
           Scores.scoreAzul++;
           this.gameObject.transform.position = respawn.position;
        }
        if(other.CompareTag("arco Azul"))
        {
            Scores.scoreRojo++;
            this.gameObject.transform.position = respawn.position;
        }
    }
}
