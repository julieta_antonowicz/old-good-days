using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class camaraFin : MonoBehaviour
{
    public TextMeshProUGUI textoGanador;

    void Update()
    {
        GameObject ganador = GameObject.FindGameObjectWithTag("sentado");
        if (ganador != null)
        {
            MoverEnCirculo mover = ganador.GetComponent<MoverEnCirculo>();
            if (mover != null)
            {
                GameManagerSilla.nombreGanador = mover.nombreJugador;
            }
        }
        textoGanador.text = "El ganador es " + GameManagerSilla.nombreGanador + " !!!";
    }
}
