using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReproducirCancionAleatoria : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip cancion;
    public float duracionMinima = 5f;
    public float duracionMaxima = 10f;

    public static bool reproducir;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        reproducir = true;
    }
    private void Update()
    {
        if (reproducir)
        {
            ReproducirCancion();
            reproducir = false;
        }
    }
    public void ReproducirCancion()
    {
        MoverEnCirculo.puedeSentarse = false;
        audioSource.clip = cancion;
        audioSource.Play();

        float duracion = Random.Range(duracionMinima, duracionMaxima);
        Invoke("DetenerCancion", duracion);
    }
    private void DetenerCancion()
    {
        audioSource.Stop();
        Debug.Log("cancion pausada");
        MoverEnCirculo.puedeSentarse = true;
        reproducir = false;
    }
}
