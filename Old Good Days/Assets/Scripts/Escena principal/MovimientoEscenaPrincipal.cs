using UnityEngine;

public class MovimientoEscenaPrincipal : MonoBehaviour
{
    public float speed = 5f; 

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontalInput, 0f, verticalInput) * speed * Time.deltaTime;

        transform.Translate(movement);
    }
}
