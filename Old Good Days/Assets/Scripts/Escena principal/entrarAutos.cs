using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class entrarAutos : MonoBehaviour
{
    public GameObject collisionText;
    public string nextSceneName;

    private bool showCollisionText = false;

    private void Update()
    {
        if (showCollisionText && Input.GetKeyDown(KeyCode.E))
        {
            SceneManager.LoadScene(nextSceneName);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            showCollisionText = true;
            collisionText.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            showCollisionText = false;
            collisionText.SetActive(false);
        }
    }
}
