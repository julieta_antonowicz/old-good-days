using UnityEngine;

public class CubeShooter : MonoBehaviour
{
    public GameObject spherePrefab; 
    public float shootForce = 10f; 
    public float shootInterval = 1f; 
    public Transform target; 

    private float lastShootTime;

    private void Start()
    {
        lastShootTime = Time.time; 
    }

    private void Update()
    {
        if (Time.time - lastShootTime >= shootInterval)
        {
            Shoot();
            lastShootTime = Time.time; 
        }
    }

    private void Shoot()
    {
        GameObject sphere = Instantiate(spherePrefab, transform.position, transform.rotation);

        Vector3 shootDirection = (target.position - transform.position).normalized;
        Rigidbody rb = sphere.GetComponent<Rigidbody>();
        rb.AddForce(shootDirection * shootForce, ForceMode.VelocityChange);

        Destroy(sphere, 1f);
    }
}

