using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Contador : MonoBehaviour
{
    public float countdownDuration = 3f;
    public TextMeshProUGUI contadorText;
    public GameObject[] cubos; // Lista de cubos que queremos activar.

    private void Start()
    {
        // Desactivar los cubos al inicio.
        foreach (GameObject cubo in cubos)
        {
            cubo.SetActive(false);
        }

        StartCoroutine(Contar());
    }

    private IEnumerator Contar()
    {
        int contador = 3;

        while (contador >= 1)
        {
            contadorText.text = contador.ToString();
            yield return new WaitForSeconds(1f);
            contador--;
        }

        yield return new WaitForSeconds(1f);

        // Activar los cubos al finalizar el contador.
        foreach (GameObject cubo in cubos)
        {
            cubo.SetActive(true);
        }

        // Despu�s de realizar las acciones adicionales, puedes desactivar el texto o el Canvas si lo prefieres.
        contadorText.gameObject.SetActive(false);
    }
}

