using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasVelocidad : MonoBehaviour, IColeccionable
{
    public float velocidadAdicional = 3000f; 
    public float duracionEfecto = 5f;

    public void Recolectar(Transform jugadorTransform)
    {
        MovimientoBase movimientoJugador = jugadorTransform.GetComponent<MovimientoBase>();
        if (movimientoJugador != null)
        {
            StartCoroutine(AplicarEfectoVelocidad(movimientoJugador, velocidadAdicional, duracionEfecto));
        }
    }

    private IEnumerator AplicarEfectoVelocidad(MovimientoBase movimientoJugador, float velocidadAdicional, float duracion)
    {
        movimientoJugador.speed += velocidadAdicional; 
        yield return new WaitForSeconds(duracion); 
        movimientoJugador.speed -= velocidadAdicional; 
    }
}
