using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerarObstaculo : MonoBehaviour, IColeccionable

{
    public GameObject obstaculo;

    private float minXJ1 = -69.7f;
    private float minYJ1 = 14.52f;
    private float minZJ1 = 248.6f;
    private float maxXJ1 = 39.3f;
    private float maxYJ1 = 14.52f;
    private float maxZJ1 = 95.8f;
    
    private float minXJ2 = -197.9f;
    private float minYJ2 = 14.52f;
    private float minZJ2 = 251.2f;
    private float maxXJ2 = -82f;
    private float maxYJ2 = 14.52f;
    private float maxZJ2 = 95.9f;

    private float selectedX;
    private float selectedY;
    private float selectedZ;

    public void Recolectar(Transform jugador)
    {
        if (jugador.CompareTag("Player 1"))
        {
            selectedX = Random.Range(minXJ1, maxXJ1);
            selectedY = Random.Range(minYJ1, maxYJ1);
            selectedZ = Random.Range(minZJ1, maxZJ1);
        }
        else if (jugador.CompareTag("Player 2"))
        {
            selectedX = Random.Range(minXJ2, maxXJ2);
            selectedY = Random.Range(minYJ2, maxYJ2);
            selectedZ = Random.Range(minZJ2, maxZJ2);
        }
        //else
        //{
        //    Debug.LogWarning("Tag del jugador no reconocida: " + jugador.tag);
        //    return; // Si el jugador no tiene una tag reconocida, salimos de la funci�n sin generar el obst�culo.
        //}

        Debug.Log("el objeto se esta situando");
        Vector3 selectedPosition = new Vector3(selectedX, selectedY, selectedZ);
        Instantiate(obstaculo, selectedPosition, Quaternion.identity);
    }
}