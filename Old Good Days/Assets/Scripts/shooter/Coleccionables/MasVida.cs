using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasVida : MonoBehaviour , IColeccionable
{
    public void Recolectar(Transform jugadorTransform)
    {
        ContadorVidas contadorVidas = jugadorTransform.GetComponent<ContadorVidas>();
        if (contadorVidas != null)
        {
            contadorVidas.AumentarVidas(1); 
        }
    }
}
