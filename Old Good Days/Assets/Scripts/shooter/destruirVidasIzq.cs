using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destruirVidasIzq : MonoBehaviour
{
    public GameObject vida1izq;
    public GameObject vida2izq;
    public GameObject vida3izq;

    private void Update()
    {
        if (ContadorVidas.vidas == 2)
        {
            Destroy(vida3izq);
        }

        if(ContadorVidas.vidas == 1)
        {
            Destroy(vida2izq);
        }

        if (ContadorVidas.vidas == 0)
        {
            Destroy(vida1izq);
        }
    }
}
