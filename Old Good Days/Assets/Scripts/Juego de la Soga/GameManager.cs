using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static int jugadoresMuertos;
    public Camera camaraJuego;
    public Camera camaraGanador;
    public static string Ganador;
    private void Update()
    {
        if (jugadoresMuertos == 3)
        {
            camaraJuego.gameObject.SetActive(false);
            camaraGanador.gameObject.SetActive(true);
        }
        
    }
}
