using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class camaraGanadorSoga : MonoBehaviour
{
    public TextMeshProUGUI textoNombreGanador;

    private void Update()
    {
        textoNombreGanador.text = GameManager.Ganador;
    }
}
