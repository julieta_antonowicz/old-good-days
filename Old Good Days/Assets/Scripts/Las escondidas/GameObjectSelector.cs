using UnityEngine;

public class GameObjectSelector : MonoBehaviour
{
    public GameObject[] gameObjects;

    private int selectedObjectIndex = 0;
    private bool canChangeObject = true;

    private void Start()
    {
        // Deshabilita todos los Game Objects al inicio del juego
        foreach (GameObject obj in gameObjects)
        {
            obj.SetActive(false);
        }

        // Habilita el primer Game Object
        gameObjects[selectedObjectIndex].SetActive(true);
    }

    private void Update()
    {
        if (canChangeObject && CompareTag("auto rojo"))
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                SelectNextGameObject();
            }
        }
        if (canChangeObject && CompareTag("auto verde"))
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                SelectNextGameObject();
            }
        }
        if (canChangeObject && CompareTag("auto amarillo"))
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                SelectNextGameObject();
            }
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            canChangeObject = !canChangeObject;
            if (canChangeObject)
            {
                SelectNextGameObject();
            }
        }
    }

    private void SelectNextGameObject()
    {
        gameObjects[selectedObjectIndex].SetActive(false);

        selectedObjectIndex++;
        if (selectedObjectIndex >= gameObjects.Length)
        {
            selectedObjectIndex = 0;
        }

        gameObjects[selectedObjectIndex].SetActive(true);
    }
}
